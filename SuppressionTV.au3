#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Compile_Both=y
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
; #INDEX# =============================================================================================================================
; Titre .........: SuppressionTV																									  =
; Auteur ........: Nicolas Bertrand																									  =
; AutoIt Version : 3.3.14.5																											  =
; Langue ......: Français																											  =
; Description ...: Ce script permet de supprimer le programme TeamViewer gratuit de l'ordinateur puis de le remplacer par celui-ci du =
; Gip Recia 
; stage juin 2019																													  =
; =====================================================================================================================================
#include <AutoItConstants.au3>
#include <File.au3>
#RequireAdmin

;Inclut le fichier TeamViewerQS.exe lors de la compilation du script 
FileInstall(@ScriptDir & "\TeamViewerQS.exe", ".\")

;Exécute la fonction
DeleteTV()

Func DeleteTV()
#Region Récupération de la valeur du contrôle du compte d'utilisateur dans la base de registre
	;Récupere la valeur du control Utilisateur (1 = Activer, 0 = Désactiver)
	$value = RegRead("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", "EnableLUA")

	If @error Then
		FLog("Imppossible de récupère la valeur de l'UAC vérifier que la clé EnableLUA dans la base de registres existe bien à cet emplacement 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System'")
	EndIf
#EndRegion

	;Vérifie si le contrôle utilisateur est activé, s'il est activé il le désactive et redémarre l'ordinateur
	If ($value = 1) Then
#Region Désactivation du contrôle de compte d'utilisateur
		;Supprime le valeur EnableLUA
		RegDelete("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", "EnableLUA")

		;Crée une valeur EnableLUA et la met à 0
		RegWrite("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", "EnableLUA", "REG_DWORD", "0")

		;Si il n'y apas eu d'erreur on relance la fonction pour passer a la suite
		If Not @error Then
			;Met à jour les données modifier dans la base de registre sans redémarrer l'ordinateur
			EnvUpdate()
			DeleteTV()
		Else
			FLog("La modification de la base de registre a échoué. Lancer le programme en tant que administrateur si vous ne l'avez pas fait")
		EndIf
#EndRegion

	;Si la valeur d'EnableLUA est à 0 il exécute ces commandes
	ElseIf ($value = 0) Then
#Region Création des variables
		$appli = "TeamViewer"
		$key = ""
		$name = ""
		$erreurB = ""
		Global $messageFin
		$i=1

		;Vérifie le type d'architecture de l'OS et définit les chemins en fonction
		if @OSArch = "X64" Then
			$search = "HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall"
			$cheminNew = "C:\Program Files (x86)\TeamViewer"
		Else
			$search = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall"
			$cheminNew = "C:\Program Files\TeamViewer"
		EndIf
#EndRegion

#Region Recherche du chemin de TeamViewer dans la base de registre
		;Parcours la liste des clés contenues dans Uninstall
		While ($name <> $appli)
			$test_key = RegEnumKey($search,$i)
			;S'il n'arrive pas à parcourir la clé il écrit un log et sort de la boucle
			If @error <> 0 then
				FLog("Le chemin de la base de registre est incorrect vérifier si le chemin "&$search&" existe bien")
				ExitLoop
			EndIf

			If StringInStr(RegRead($search & "\" & $test_key, "DisplayName"), $appli) Then
				$name=$appli
				$key = RegRead($search & "\" & $test_key, "UninstallString")
				ExitLoop
			EndIf
			$i+=1
		WEnd

		If $name = "" Then
			FLog("Il n'y a pas d'application nommée " & $appli & " dans la base de registre")
		EndIf

#EndRegion

#Region Désinstallation de TeamViewer
		;Ferme tous les processus TeamViewer en cours
		ProcessClose ("TeamViewer.exe")
		ProcessClose ("TeamViewer_Service.exe")
		ProcessClose ("TeamViewer_Desktop.exe")
		ProcessClose ("mshta.exe")

		;Vérifie si TeamViewer est un msi ou un exe et le désinstalle suivant sont type
		If StringInStr($key, "MsiExec.exe") Then
			;Lance la désinstallation de TeamViewer msi
			RunWait("MsiExec.exe /qb /X"& $test_key & " REBOOT=ReallySuppress /norestart", "", @SW_HIDE)
		ElseIf StringInStr($key, "Uninstall.exe") Then
			;Lance la désinstallation de TeamViewer exe
			RunWait($key & " /S", "", @SW_HIDE)
		EndIf
		;Pause de 4000 miliseconde
		Sleep(4000)
#EndRegion

#Region Installation du nouveau TeamViewer
		;Crée le dossier contenu dans la variable $chemin
		DirCreate($cheminNew)
		;Copie le nouveau TeamViewer contenu dans le script dans le dossier prècèdement crée
		$messageFin = FileCopy(".\TeamViewerQS.exe", $cheminNew)
		;Crée une icone de TeamViewer sur le bureau
		FileCreateShortcut($cheminNew & "\TeamViewerQS.exe",  @DesktopDir & "\TeamViewer.lnk")
#EndRegion

#Region Activation du contrôle de compte d'utilisateur
		;Supprime le valeur EnableLUA
		RegDelete("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", "EnableLUA")
		;Crée une valeur EnableLUA et la met à 1 pour réactiver le contrôle utilisateur
		RegWrite("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", "EnableLUA", "REG_DWORD", "1")
		;Pause de 2000 miliseconde
		Sleep(2000)
		;Met à jour les données modifier dans la base de registre sans redémarrer l'ordinateur
		EnvUpdate()
#EndRegion
	EndIf
EndFunc

If $messageFin = 1 Then
	MsgBox(0, "Installation réussie","L'instalation de TeamViewer à reussie")
ElseIf $messageFin = 0 Then
	MsgBox(16, "Erreur","Une erreur c'est produit lords de l'installation de TeamViewer, plus d'information dans le fichier Erreur.log")
EndIf
;Fonction qui permet d'écrire dans un fichier log
Func FLog($message)
	$log = FileOpen(@ScriptDir & "\Erreur.log", 1)
	_FileWriteLog($log, $message)
	FileClose($log)
EndFunc

Exit